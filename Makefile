cxxnoq: src/cxxnoq.cpp build/expr.o
	g++ src/cxxnoq.cpp build/expr.o -o cxxnoq

build/expr.o: src/expr.cpp src/expr.hpp
	g++ src/expr.cpp -c -o build/expr.o

clean:
	rm build/*.o cxxnoq
