#include "expr.hpp"
#include <iostream>

expr expr::clone()
{
	expr ret{any, child};
	for(auto &c : ret.child)
		c = c.clone();
	return ret;
}

void expr::print(int tabn)
{
	for(int i=0; i < tabn; i++ )
		std::cout << '\t';
	std::cout << "|";
	std::cout << std::endl;
	for(int i=0; i < tabn; i++ )
		std::cout << '\t';
	std::cout << "- " << any << std::endl;
	for(auto &c: child)
		c.print(tabn+1);
}

bool expr::operator==(expr &e)
{
	if( any != e.any || child.size() != e.child.size() )
		return false;
	else
		for(int i=0; i < child.size(); i++ )
		{
			if( child[i] != e.child[i] )
				return false;
		}
	return true;
}

bool expr::match(expr &e, std::unordered_map<std::string, expr *> &map )
{
	if( child.size() )
	{
		if( any != e.any || child.size() != e.child.size() )
			return false;
		else
			for(int i=0; i < child.size(); i++ )
			{
				if( !child[i].match(e.child[i], map ) )
					return false;
			}
		return true;
	}
	else
	{
		if( map.count(any) )
			return *map[any] == *this;
		else
		{
			map[any] = &e;
			return true;
		}
	}	
}

expr expr::subst(std::unordered_map<std::string, expr *> &map )
{
	expr ret{any, child};
	for(auto &c : ret.child)
	{
		if( c.child.size() )
			c = c.subst(map);
		else
			if( map.count(c.any) )
				c = map[c.any]->clone();
			else
				c = expr{c.any, {}};
	}
	return ret;
}

expr expr::apply(expr &head, expr &body )
{
	std::unordered_map<std::string, expr *> map;
	if( head.match(*this, map ) )
		return body.subst(map);
	else
		return *this;
}

inline bool expr::operator!=(expr &e)
{ return !((*this) == e); }
