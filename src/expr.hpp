#pragma once
#ifndef __EXPR_HPP__
#define __EXPR_HPP__

#include <string>
#include <vector>
#include <unordered_map>

struct expr
{
	std::string any;
	std::vector<expr> child;
	expr clone();
	void print(int tabn=0);	
	bool operator==(expr &e);	
	bool match(expr &e, std::unordered_map<std::string, expr *> &map );	
	expr subst(std::unordered_map<std::string, expr *> &map );
	expr apply(expr &head, expr &body );	
	inline bool operator!=(expr &e);
};

#endif
