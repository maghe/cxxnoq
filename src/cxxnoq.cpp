#include <any>
#include <iostream>
#include "expr.hpp"

int main()
{
	std::vector<expr> symstack;
	std::vector<size_t> pstack;
	std::unordered_map<std::string, expr> rules;
	for(std::string input; std::cin >> input, input != "END"; )
	{
		if( input == "!del" )
		{
			if( symstack.size() )
				symstack.pop_back();
		}
		else if( input == "!print" )
		{
			if (symstack.size() )
				symstack.back().print();
		}
		else if( input == "(" )
			pstack.push_back(symstack.size() );
		else if( input == ")" )
		{
			if( pstack.size() == 0 )
				pstack.push_back(0);
			if( pstack.back() == symstack.size() )
				goto syntax_err;
			std::cin >> input;
			if( input == "END" )
				break;
			if( input == ")" )
				goto syntax_err;
			int start = pstack.back();
			pstack.pop_back();
			std::vector<expr> copy{symstack.begin()+start,symstack.end()};
			if( input == "->" )
			{
				if( copy.size() < 2 )
					goto syntax_err;
				else
				{
					std::cin >> input;
					//TODO: no weird names please
					rules[input] = expr{input, copy };
					std::cout << "rule added" << std::endl;
					symstack.erase(symstack.begin()+start, symstack.end() );
				}
			}
			else if( rules.count(input) )
			{
				if( copy.size() != 1 )
					goto syntax_err;
				symstack.erase(symstack.begin()+start, symstack.end() );
				symstack.push_back(copy[0].apply(rules[input].child[0], rules[input].child[1]));
				/*for(auto &e : symstack)
				{
					e.print();
					std::cout << "-------" << std::endl;
				}*/
			}
			else
			{
				symstack.erase(symstack.begin()+start, symstack.end() );
				symstack.push_back(expr{input, copy });
			}
		}
		else
			symstack.push_back(expr{input, {} });	
		/*if( symstack.size() > 1 )
			std::cout << "== : " << ( symstack.front() == symstack.back() ) << std::endl;*/
		continue;
		syntax_err:
			std::cout << "Syntax error" << std::endl;
			return 1;
	}
}
